# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Install sudo and maintain sudoers file 
#

class sudo {

    package { "sudo": ensure => 'latest' }

    file { "/etc/sudoers.d/staff": 
        ensure  => present,
        owner   => 0,
        group   => 0,
        mode    => 440,
        source  => 'puppet:///modules/sudo/staff',
        require => Package['sudo'],
    }
}

# vim: noai:ts=4:sw=4:
