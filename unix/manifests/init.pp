#
#
##
#
##
#
#

class unix (
    $mgt_server     = hiera('mgt_server', undef),
){

    # Basic packages for Debian/Ubuntu hosts
    package { "apticron": 
        ensure  => 'installed',
    }

    # Setup hosts file
    file { "/etc/hosts":
        ensure  => 'present',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('unix/etc/hosts.erb'),
    }

    # Setup resolv.conf
    file { "/etc/resolv.conf":
        ensure  => file,
        owner   => 0,
        group   => 0,
        mode    => '0644',
        content => template('unix/etc/resolv.conf.erb'),
    }


    # Setup issue files
    file { [ "/etc/issue", "/etc/issue.net" ]:
        ensure  => file,
        owner   => 0,
        group   => 0,
        mode    => '0644',
        source  => 'puppet:///modules/unix/issue.txt',
    }

}

# vim: noai:ts=4:sw=4:
