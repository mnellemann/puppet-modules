class comin::collectd {

collectd::plugin::tail::file { 'auth-log':
    filename => '/var/log/auth.log',
    instance => 'auth',
    matches  => [
      {
        regex    => '\\<sshd[^:]*: Invalid user [^ ]+ from\\>',
        dstype   => 'CounterInc',
        type     => 'counter',
        instance => 'sshd-invalid_user',
      }
    ]
  }


}
