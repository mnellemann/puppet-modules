# Copyright (c) 2014, Mark Nellemann

class comin::sysctl::general { 

    # General tweaks suitable for all servers
    file { "/etc/sysctl.d/20-general.conf":
        ensure  => present,
        owner   => 0,
        group   => 0,
        mode    => '0644',
        source  => 'puppet:///modules/comin/sysctl/general.conf',
        notify  => Exec["sysctl-20"],
        audit   => 'content',
    }

    exec { "sysctl-20":
        command     => "/sbin/sysctl -p /etc/sysctl.d/20-general.conf",
        refreshonly => true,
    }


}
    
# vim: set noai ts=4 sw=4:
