#!/bin/sh

DEST=/var/backups
test -d $DEST || exit 1

for name in gzip mysql mysqldump; do
    if ! [ "$(which ${name})" ]; then
        echo "Error - Command \"${name}\" is not available, aborting."
        exit 1
    fi
done


# Make sure destination exists
mysql_dest="${DEST}/mysql"
mkdir -p $mysql_dest

# Set defaults file with login info
if [ -f "/etc/mysql/custom.cnf" ]; then
    mycnf="/etc/mysql/custom.cnf"
else
    mycnf="/etc/mysql/debian.cnf"
fi

# get all database listing
DBS="$(mysql --defaults-file=$mycnf -Bse 'show databases')"

# start to dump database one by one
for db in $DBS; do
    echo "Dumping MySQL DB: $db"
    mysqldump --defaults-file=$mycnf --ignore-table=mysql.event --single-transaction --databases $db | gzip -9 > "${mysql_dest}/${db}.sql.gz"
done


exit 0
