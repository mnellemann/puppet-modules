class comin::burp {

    # Daily cron job
    file { "/etc/cron.daily/burp":
        ensure  => present,
        source  => 'puppet:///modules/comin/burp/burp-cron.sh',
        owner   => 0,
        group   => 0,
        mode    => 0755,
    }

    # Burp Pre-Backup Script
    file { "/usr/local/bin/burp-pre-backup.sh":
        ensure  => present,
        source  => 'puppet:///modules/comin/burp/burp-pre-backup.sh',
        owner   => 0,
        group   => 0,
        mode    => 0740,
    }

    # MySQL Backup Script
    file { "/usr/local/bin/mysql-backup.sh":
        ensure  => present,
        source  => 'puppet:///modules/comin/burp/mysql-backup.sh',
        owner   => 0,
        group   => 0,
        mode    => 0740,
    }

    # PostgreSQL Backup Script
    file { "/usr/local/bin/postgresql-backup.sh":
        ensure  => absent,
        source  => 'puppet:///modules/comin/burp/postgresql-backup.sh',
        owner   => 0,
        group   => 0,
        mode    => 0740,
    }
    
}
