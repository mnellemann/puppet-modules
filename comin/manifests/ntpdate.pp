class comin::ntpdate {

    package{ "ntpdate":
        ensure  => installed,
    }

    cron { "ntpdate_regular":
        ensure  => present,
        command => '/usr/sbin/ntpdate -u dk.pool.ntp.org >/dev/null 2>&1',
        user    => 'root',
        minute  => '59',
        require  => Package['ntpdate'],
    }

    cron { "ntpdate_reboot":
        ensure  => present,
        command => '/usr/sbin/ntpdate -u dk.pool.ntp.org >/dev/null 2>&1',
        user    => 'root',
        special => 'reboot',
        require => Package['ntpdate'],
    }

}
