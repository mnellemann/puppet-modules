# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Puppet module that installs snmpd 
#

# TODO: params

# Master class
class snmpd(
    $allow_rw       = hiera('snmpd::allow_rw', 'localhost'),
    $allow_ro       = hiera('snmpd::allow_ro', 'localhost'),
    $allow_sys      = hiera('snmpd::allow_sys', '10.0.0.0/8'),
    $sys_location   = hiera('snmpd::sys_location', 'Unknown Location'),
    $sys_contact    = hiera('snmpd::sys_contact', 'Unknown Contact'),
) {

    case $::operatingsystem {
        "FreeBSD": {
            $provider       = "freebsd"
            $package        = "snmpd"
            $service        = "snmpd"
            $conf           = "/usr/local/etc/snmp/snmpd.conf"
            $owner          = "root"
            $group          = "wheel"
        }
        /Debian|Ubuntu/: {
            $provider       = "apt"
            $package        = "snmpd"
            $service        = "snmpd"
            $conf           = "/etc/snmp/snmpd.conf"
            $owner          = "root"
            $group          = "root"

            include snmpd::default
        }
        default: { fail("Unsupported OS $::operatingsystem") }
    }

    # Include sub classes for all OSes
    include ::snmpd::install, ::snmpd::service, ::snmpd::config
}

# Install class
class snmpd::install inherits snmpd {
    package { $package: ensure => present, provider => $provider }

    # This file is for Observium and doesn't really belong here, but is referenced from snmpd.conf
    file { "/usr/local/bin/distro":
        ensure  => 'present',
        owner   => "$owner",
        group   => "$group",
        mode    => '0755',
        source  => 'puppet:///modules/snmpd/distro.sh',
    }
}

# Configuration class
class snmpd::config inherits snmpd {

    File {
        require => Class["snmpd::install"],
        notify  => Class["snmpd::service"],
        owner   => "$owner",
        group   => "$group",
        mode    => 644
    }

    file { "$conf":
        owner => "$owner",
        group => "$group",
        mode  => 640,
        content => template('snmpd/snmpd.conf.erb'),
        require => Package["$package"];
    }

    
}

# Default startup class for Debian / Ubuntu
class snmpd::default inherits snmpd {
    File {
        require => Class["snmpd::install"],
        notify  => Class["snmpd::service"],
        owner   => "$owner",
        group   => "$group",
        mode    => 644
    }

    file { "/etc/default/snmpd":
        owner => "$owner",
        group => "$group",
        mode => 644,
        source => 'puppet:///modules/snmpd/default-snmpd',
        require => Package["$package"];
    }
}


# Service class
class snmpd::service inherits snmpd {

    service { "$service":
        enable  => true,
        hasrestart => true,
        require => Class["snmpd::config"];
    }
}



# vim: noai:ts=4:sw=4:
