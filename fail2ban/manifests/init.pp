# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Puppet module that installs fail2ban 
#

# TODO: params

# Master class
class fail2ban(
    $ignore      = hiera('fail2ban::ignore', '127.0.0.1'),
    $email       = hiera('fail2ban::email', 'root'),
    $bantime     = hiera('fail2ban::bantime', '1800'),
    $maxretry    = hiera('fail2ban::maxretry', '3'),
) {

    case $::osfamily {
        'Debian': {
            $package        = "fail2ban"
            $service        = "fail2ban"
        }
        default: { fail("Unsupported OS $::osfamily") }
    }

    # Include sub classes for all OSes
    include ::fail2ban::install, ::fail2ban::service, ::fail2ban::config

}


# Install class
class fail2ban::install inherits fail2ban {
    package { $package: ensure => installed }
}


# Configuration class
class fail2ban::config inherits fail2ban {

    File {
        require => Class["fail2ban::install"],
        notify  => Class["fail2ban::service"],
        owner   => "$owner",
        group   => "$group",
        mode    => 644
    }

    file { "/etc/fail2ban/fail2ban.conf":
        owner   => "root",
        group   => "root",
        mode    => '0644',
        source => 'puppet:///modules/fail2ban/fail2ban.conf',
        require => Package["$package"],
    }

    file { "/etc/fail2ban/jail.local":
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('fail2ban/jail.local.erb'),
        require => Package["$package"],
    }
    
}


# Service class
class fail2ban::service inherits fail2ban {

    service { "$service":
        enable  => true,
        hasrestart => true,
        require => Class["fail2ban::config"];
    }
}


# vim: noai:ts=4:sw=4:
