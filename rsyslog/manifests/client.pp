# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Puppet module that sets up rsyslog to log to a centralized syslog server
#

# TODO: filter severities or destination to include or exclude from sending to syslog server


class rsyslog::client (
    $enable = hiera('rsyslog::client::enable', true),
    $server = hiera('rsyslog::client::server', "syslog"),
) {

    # List of packages to install
    $packages = [ 'rsyslog' ]
    $service = "rsyslog"

    # To be able to include this class on all nodes, including the syslog-server, you can
    # set enable=false with hiera for the syslog-server node and not worry it will mess up.
    if($enable) {
        include "::rsyslog::client::install", "::rsyslog::client::service", "::rsyslog::client::config"
    }
}


class rsyslog::client::install inherits rsyslog::client {
    package { $packages: ensure => 'installed', }
}


class rsyslog::client::service inherits rsyslog::client {
    
    service { "${service}":
        enable  => true,
        ensure  => 'running',
        require     => Class["rsyslog::client::config"]
    }
        
}


class rsyslog::client::config inherits rsyslog::client {

    File {
        require => Class["rsyslog::client::install"],
        notify  => Class["rsyslog::client::service"],
        owner   => "root",
        group   => "root",
        mode    => '0644',
    }

    file { "/etc/rsyslog.d/client.conf": 
        ensure  => 'present',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => template('rsyslog/client.conf.erb'),
    }

}

# vim: noai:ts=4:sw=4:
