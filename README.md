# Puppet Modules

This is a collection of the Puppet modules that I have made and use across different systems.

I use **hiera** for external configuration and all modules are tailored specifically for this.

## SSL Cert mismatch

     puppet cert generate puppet --dns_alt_names=puppet.mydomain.com,other-name.mydomain.com
