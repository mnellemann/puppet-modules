#!/bin/sh

###
# This script is run prior to BURP backup
#
# Usacases could be dumping mysql databases or
# subversion repositories.
# 

DEST="/var/backups"
MYSQLHOME="/var/lib/mysql"
PGSQLHOME="/var/lib/postgresql"

#
# Function that dumps system infor
#

if [ -f /etc/debian_version ]; then

        # Make sure destination exist
        sys_dest="${DEST}/sys"
        mkdir -p $sys_dest

        # TODO: Collect information about system: installed packages, hwinfo, etc.

        # packagesfile = /var/backups/dpkg-selections.txt
        /usr/bin/dpkg --get-selections > /var/backups/sys/debconfsel.txt

        # partitionsfile = /var/backups/partitions.__star__.txt
        # hardwarefile = /var/backups/hardware.txt

fi


#
# Function that dumps MySQL databases 
#

if [ -d $MYSQLHOME ]; then
    test -x /usr/local/bin/mysql-backup.sh && /usr/local/bin/mysql-backup.sh
fi


#
# Function that dumps PgSQL databases 
#

if [ -d $PGSQLHOME ]; then
    test -x /usr/sbin/autopostgresqlbackup && /usr/sbin/autopostgresqlbackup
    #test -x /usr/local/bin/postgresql-backup.sh && /usr/local/bin/postgresql-backup.sh
fi



exit 0
