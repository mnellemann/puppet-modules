# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Install and maintain puppet agent
#

# TODO: make configurable
# TODO: make osfamily aware
# TODO: run as daemon
# TODO: run from cron


class puppet::agent {
    include "::puppet::agent::install", "::puppet::agent::config", "::puppet::agent::service"
}


class puppet::agent::install {
}


class puppet::agent::service {
    
    service { "puppet":
        enable  => true,
        ensure  => 'running',
        hasstatus   => true,
        hasrestart  => true,
        require => Class["puppet::agent::config"]

    }
        
}


class puppet::agent::config {

    File {
        require => Class["puppet::agent::install"],
        notify  => Class["puppet::agent::service"],
        owner   => "root",
        group   => "root",
        mode    => '0644',
    }

    file { "/etc/default/puppet": 
        ensure  => 'present',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/puppet/agent/default-puppet',
    }

    file { "/etc/puppet/puppet.conf":
        ensure  => 'present',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        source  => 'puppet:///modules/puppet/agent/puppet.conf',
    }
}

# vim: noai:ts=4:sw=4:
