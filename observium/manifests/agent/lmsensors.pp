# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Install the lmsensors agent monitoring files
#

#
# TODO:  depend on lmsensors
#

class observium::agent::lmsensors {

    # Make sure we have Observium Agent installed
    if ! defined(Class['::observium::agent']) {
        include '::observium::agent'
    }

    package { "lm-sensors": ensure => 'installed', }

    file { "/usr/lib/observium_agent/local/lmsensors":
        ensure  => file,
        owner   => 0,
        group   => 0,
        mode    => '0755',
        source  => 'puppet:///modules/observium/agent/local/lmsensors',
    }

}

# vim: noai:ts=4:sw=4:
