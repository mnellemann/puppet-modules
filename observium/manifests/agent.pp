# Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#
# Puppet module that installs observium agent
#


# Main Class
class observium::agent(
    $servers    = hiera('observium::agent::servers', undef),
    $port       = hiera('observium::agent::port', 36602),
) {

    # Packages
    $packages = [ 'xinetd' ]
    
    include observium::agent::install, observium::agent::config, observium::agent::service, observium::agent::ufw
}


# Agent Install 
class observium::agent::install inherits observium::agent {

    package { $packages : ensure => installed }

	file { "/usr/bin/observium_agent":
		ensure	=> 'present',
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0755',
		source	=> 'puppet:///modules/observium/agent/observium_agent',
	}

	file { "/usr/lib/observium_agent":
		ensure	=> 'directory',
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0755',
		before	=> File['/usr/lib/observium_agent/local'],
	}

	file { "/usr/lib/observium_agent/local":
		ensure	=> 'directory',
		owner	=> 'root',
		group	=> 'root',
		mode 	=> '0755',
	}
}


# Agent Firewall
class observium::agent::ufw inherits observium::agent {

    exec { "allow-observium":
        command => "/usr/sbin/ufw allow proto tcp from ${server}/32 to any port 36602",
        unless  => "[ ! -x /usr/sbin/ufw ] || [ $(/usr/sbin/ufw status | /bin/grep -e \"36602/tcp.*ALLOW.*${server}\\|Status: inactive\" | wc -l) -gt 0 ]"
    }

}


# Agent Service
class observium::agent::service inherits observium::agent {

    service { "xinetd":
        ensure  => running,
        enable  => true,
        hasstatus => false,
        hasrestart => true,
        require => Class["observium::agent::config"]
    }
}


# Agent Config
class observium::agent::config {

    File {
        require => Class["observium::agent::install"],
        notify  => Class["observium::agent::service"],
        owner   => "root",
        group   => "root",
        mode    => 644
    }
   
    file { "/etc/xinetd.d/observium_agent":
        owner   => 'root',
        group   => 'root',
        mode    => 0644,
        content => template('observium/agent/xinetd-observium_agent.conf.erb'),
    }
}

# vim: noai:ts=4:sw=4:
