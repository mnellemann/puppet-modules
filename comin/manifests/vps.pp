class comin::vps {

    # TODO: Make some checks to make sure that the folder exist
    file { "/home/admin/web/${fqdn}/public_html/status.php":
        ensure  => present,
        source  => 'puppet:///modules/comin/vps/status.php',
    }


    # Set UTF-8 as default for MySQL
    exec { "check_mysql_conf":
        command => '/bin/true',
        onlyif  => '/usr/bin/test -d /etc/mysql/conf.d',
    }
    file { "/etc/mysql/conf.d/utf8.cnf":
        ensure  => present,
        content => "[mysqld]\ncharacter_set_client = utf8\ncharacter_set_server = utf8\n",
        require => Exec["check_mysql_conf"],
    }

}
